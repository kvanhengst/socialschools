$(function() {
	var fillEqualColumns = function() {
		$('.equal-columns').each(function() {
			$(this).find('.fill-me').height(0);
			var cols = $(this).find('*[class^=span]'),
				heights = cols.map(function() { return $(this).height(); }),
				max = Math.max.apply(null, heights);
			console.log(cols);
			cols.each(function(i) {
				$(this).find('.fill-me').height(max - heights[i]);
			});
		});
	};
	fillEqualColumns();
	$(window).load(fillEqualColumns).resize(fillEqualColumns);
});